# Sprint 1			Lab 2


## Java syntax, OOP, Exceptions and Serialization

(Thời gian: 6h)

### Yêu cầu 1: 

- Từ project có sẵn

- Hoàn thiện project (class MyUserData extends UserData)

- Đoc user data từ file [input.data] (data đã được serialize dưới dạng ArrayList<User>)

- Thông tin User bao gồm: Id, Name, Year, Email

- Hiển thị danh sách tất cả User

### Yêu cầu 2: 

- Tìm kiếm User theo Id

### Yêu cầu 3: 

- Lưu kết quả tìm kiếm ở trên vào [output.data] 

### Yêu cầu 4: 

- Xử lý Exceptions cho các trường hợp đọc file, lưu file, …