package java_training_lab2;

import java.util.List;

/**
 * @author ...
 * abstract UserData
 *
 */
public abstract class UserData {
	
	/**
	 * the user data as list
	 */
	List<User> listUser;
	
	/**
	 * Get all user
	 * @return user data 
	 */
	abstract List<User> GetListUser();
	
	/**
	 * Find the user by the id
	 * @param id
	 * @return user match id
	 */
	abstract User FindUserById(int id);
	
	/**
	 * Display all user info to console screen
	 */
	abstract void ShowAllUserInfo();
	
	/**
	 * Read data from [input.data] file
	 * Data is serialization as ArrayList<User>
	 * @param fileName
	 * @return
	 */
	abstract Boolean ImportUserData(String fileName);
}
