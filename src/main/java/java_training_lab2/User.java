package java_training_lab2;

/**
 * @author ...
 * interface User
 *
 */
public class User implements java.io.Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -3599844947580689724L;
	
	int Id;
	int Year;
	String Name;
	String Email;
	
	public User()
	{		
	}
	
	public User(int id, int year, String name, String email)
	{
		Id = id;
		Year = year;
		Name = name;
		Email = email;
	}

	public int getId() {
		return Id;
	}

	public void setId(int id) {
		Id = id;
	}

	public int getYear() {
		return Year;
	}

	public void setYear(int year) {
		Year = year;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public String getEmail() {
		return Email;
	}

	public void setEmail(String email) {
		Email = email;
	}
	
}
