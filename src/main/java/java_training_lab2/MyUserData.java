package java_training_lab2;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.List;

public class MyUserData extends UserData implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1403093667138275469L;
	List<User> getListUser;

	public static void main(String[] args) throws ClassNotFoundException {
		MyUserData listUser = new MyUserData();
		listUser.ImportUserData("input.data");
		listUser.ShowAllUserInfo();

		// find User by id
		System.out.println("-------------------------------------------------------");
		User user = listUser.FindUserById(1);
		System.out
				.println("Id: " + user.Id + ", Name: " + user.Name + ", Year: " + user.Year + ", Email: " + user.Email);
		
		// export user data
		System.out.println("-------------------------------------------------------");
		listUser.ExportUserData(listUser, "output.data");
	}

	@Override
	List<User> GetListUser() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	User FindUserById(int id) {
		if (this.getListUser != null) {
			for (User user : this.getListUser) {
				if (user.Id == id) {
					return user;
				}
			}
		}
		return null;
	}

	@Override
	void ShowAllUserInfo() {
		if (this.getListUser != null) {
			this.getListUser.forEach((user) -> {
				System.out.println("Id: " + user.Id);
				System.out.println("Name: " + user.Name);
				System.out.println("Year: " + user.Year);
				System.out.println("Email: " + user.Email);
			});
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	Boolean ImportUserData(String fileName) {
		try {
			FileInputStream fileIn = new FileInputStream(fileName);
			ObjectInputStream in = new ObjectInputStream(fileIn);
			this.getListUser = (List<User>) in.readObject();
			in.close();
			fileIn.close();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	Boolean ExportUserData(MyUserData listUser, String fileName) throws ClassNotFoundException {
		try {
			if (this.getListUser != null) {
				FileOutputStream fileOut = new FileOutputStream(fileName);
				ObjectOutputStream out = new ObjectOutputStream(fileOut);
				out.writeObject(listUser);
				fileOut.close();
				out.close();
				System.out.printf("Serialized data is saved !");
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

}
